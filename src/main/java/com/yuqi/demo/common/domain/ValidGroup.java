package com.yuqi.demo.common.domain;

import javax.validation.GroupSequence;

/**
 * 效验分组
 *
 * @author yuqi
 * @date 2021-02-04
 */
public class ValidGroup {
    /**
     * 新增效验分组
     */
    public interface AddGroup {
    }

    /**
     * 更新效验分组
     */
    public interface UpdateGroup {
    }

    /**
     * 删除效验分组
     */
    public interface DeleteGroup {
    }

    /**
     * 修改状态效验分组
     */
    public interface ChangeStatusGroup {
    }

    /**
     * 自定义效验分组
     * 定义校验顺序，如果AddGroup组失败，则UpdateGroup组不会再校验
     */
    @GroupSequence({AddGroup.class, UpdateGroup.class})
    public interface Group {
    }
}
